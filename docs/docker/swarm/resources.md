`resources` configures physical resource constraints for container to run on platform.

[Compose file version 3 reference | Docker Documentation](https://docs.docker.com/compose/compose-file/compose-file-v3/#resources)

- `limits`: The platform MUST prevent container to allocate more
- `reservations`: The platform MUST guarantee container can allocate at least the configured amount

```
deploy:
  resources:
    limits: {cpus: '1', memory: 4608M}
	  reservations: {cpus: '1', memory: 4608M}
```

- `cpus` configures a limit or reservation for how much of the available CPU resources (as number of cores) a container can use.
- `memory` memory configures a limit or reservation on the amount of memory a container can allocate, set as a string expressing a [byte value](https://github.com/compose-spec/compose-spec/blob/master/spec.md#specifying-byte-values).
