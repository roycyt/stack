# Setup

## Ubuntu 20.04 LTS (Focal Fossa)

確認系統的 Python 3 有 venv 模組：

```
python3 -m venv -h
```

否則，需要安裝 `python3-venv` 套件。

建立 Ansible 的執行環境：

```
python3 -m venv ~/venv/ansible
source ~/venv/ansible/bin/activate
python -m pip install -U pip
python -m pip install -U ansible
python -m pip install -U "ansible-lint[yamllint]"
```

退出 Ansible 的執行環境：

```
deactivate
```
